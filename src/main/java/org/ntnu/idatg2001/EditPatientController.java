package org.ntnu.idatg2001;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import org.apache.log4j.Logger;
import java.net.URL;
import java.util.ResourceBundle;

/**
 * The type Edit patient controller.
 */
public class EditPatientController implements Initializable {

    /**
     * The First name.
     */
    @FXML
    TextField firstName;
    /**
     * The Last name.
     */
    @FXML
    TextField lastName;
    /**
     * The General practitioner.
     */
    @FXML
    TextField generalPractitioner;
    /**
     * The Social security number.
     */
    @FXML
    TextField socialSecurityNumber;
    /**
     * The Diagnosis.
     */
    @FXML
    TextField diagnosis;
    /**
     * The Ok button.
     */
    @FXML
    Button okButton;
    /**
     * The Cancel button.
     */
    @FXML
    Button cancelButton;

    /**
     * The Patient.
     */
    Patient patient;

    /**
     * The Alert.
     */
    Alert alert;

    private static final Logger LOGGER = Logger.getLogger(EditPatientController.class.getName());

    /**
     * The constant statusMessage used to send a status message to the main controller.
     */
    public static String statusMessage;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        LOGGER.info("Initializing EditPatientController.");
        statusMessage = "Patient not edited.";
        try {
            patient = PrimaryController.getSelectedPatient();
            firstName.setText(patient.getFirstName());
            lastName.setText(patient.getLastName());
            generalPractitioner.setText(patient.getGeneralPractitioner());
            socialSecurityNumber.setText(patient.getSocialSecurityNumber());
            diagnosis.setText(patient.getDiagnosis());
        }catch (IllegalArgumentException | NullPointerException nu) {
            LOGGER.error("NullPointerException or IllegalArgumentException while trying to initialize edit patient controller.  Error message is: " + nu.getMessage());
        }
    }

    /**
     * Edit patient in list. Sets the different fields in patient according to the information given in the labels.
     */
    public void editPatientInList() {
        LOGGER.info("Editing patient in list.");
        try {
            patient.setFirstName(firstName.getText());
            patient.setLastName(lastName.getText());
            patient.setGeneralPractitioner(generalPractitioner.getText());
            patient.setSocialSecurityNumber(socialSecurityNumber.getText());
            patient.setDiagnosis(diagnosis.getText());
            statusMessage = "Patient edited.";
        } catch (IllegalArgumentException | NullPointerException nu) {
            LOGGER.error("NullPointerException or IllegalArgumentException while trying to edit patient.  Error message is: " + nu.getMessage());
            alert = new Alert(Alert.AlertType.ERROR);
            alert.setHeaderText("Error while trying to edit patient.");
            alert.setContentText("Patient's first and last name cannot be blank.");
            alert.show();
        }
        closeStage();
    }

    /**
     * Close stage.
     */
    public void closeStage() {
        LOGGER.info("Closing Edit Patient stage.");
        Stage stage = (Stage) cancelButton.getScene().getWindow();
        stage.close();
    }


}