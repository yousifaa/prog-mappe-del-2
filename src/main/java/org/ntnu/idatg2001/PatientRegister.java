package org.ntnu.idatg2001;

import java.util.*;

/**
 * The type Patient register.
 */
public class PatientRegister {
    private ArrayList<Patient> patientList;

    /**
     * Instantiates a new Patient register ArrayList.
     */
    public PatientRegister() {
        this.patientList=new ArrayList<>();
    }

    /**
     * Gets patient list.
     *
     * @return the patient list
     */
    public List<Patient> getPatientList() {
        return patientList;
    }

    /**
     * Sets patient list.
     *
     * @param patientList the patient list
     */
    public void setPatientList(ArrayList<Patient> patientList) {
        this.patientList = patientList;
    }

    /**
     * Method to add patient to the patientRegister ArrayList.
     *
     * @param firstName            the first name
     * @param lastName             the last name
     * @param generalPractitioner  the general practitioner
     * @param socialSecurityNumber the social security number
     * @param diagnosis            the diagnosis
     */
    public void addPatient(String firstName, String lastName, String generalPractitioner, String socialSecurityNumber, String diagnosis) {
        patientList.add(new Patient(firstName, lastName, generalPractitioner, socialSecurityNumber, diagnosis));
    }

}
