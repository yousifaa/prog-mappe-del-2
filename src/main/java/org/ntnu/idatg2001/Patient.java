package org.ntnu.idatg2001;

/**
 * The type Patient.
 */
public class Patient {
    /**
     * The First name.
     */
    String firstName;
    /**
     * The Last name.
     */
    String lastName;
    /**
     * The General practitioner.
     */
    String generalPractitioner;
    /**
     * The Social security number.
     */
    String socialSecurityNumber;
    /**
     * The Diagnosis.
     */
    String diagnosis;

    /**
     * Instantiates a new Patient. Uses setters in constructors for robust object creation.
     *
     * @param firstName            the first name
     * @param lastName             the last name
     * @param generalPractitioner  the general practitioner
     * @param socialSecurityNumber the social security number
     * @param diagnosis            the diagnosis
     */
    public Patient(String firstName, String lastName, String generalPractitioner, String socialSecurityNumber, String diagnosis){
        setFirstName(firstName);
        setLastName(lastName);
        setGeneralPractitioner(generalPractitioner);
        setSocialSecurityNumber(socialSecurityNumber);
        setDiagnosis(diagnosis);
    }

    /**
     * Gets first name.
     *
     * @return the first name
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * Sets first name. Throws exceptions when firstName is null or blank.
     *
     * @param firstName the first name
     */
    public void setFirstName(String firstName) {
        if(firstName == null) {
            throw new NullPointerException("First name cannot be null.");
        } else if (firstName.isBlank()) {
            throw new IllegalArgumentException("First name cannot be blank.");
        }
        this.firstName = firstName;
    }

    /**
     * Gets last name.
     *
     * @return the last name
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * Sets last name. Throws exceptions when lastName is null or blank.
     *
     * @param lastName the last name
     */
    public void setLastName(String lastName) {
        if(lastName == null) {
            throw new NullPointerException("First name cannot be null.");
        } else if (lastName.isBlank()) {
            throw new IllegalArgumentException("First name cannot be blank.");
        }
        this.lastName = lastName;
    }

    /**
     * Gets general practitioner.
     *
     * @return the general practitioner
     */
    public String getGeneralPractitioner() {
        return generalPractitioner;
    }

    /**
     * Sets general practitioner. Throws exception when generalPractitioner is null.
     *
     * @param generalPractitioner the general practitioner
     */
    public void setGeneralPractitioner(String generalPractitioner) {
        if(generalPractitioner != null) {
            this.generalPractitioner= generalPractitioner;
        } else {throw new IllegalArgumentException("General practitioner cannot be null.");}
    }


    /**
     * Gets social security number.
     *
     * @return the social security number
     */
    public String getSocialSecurityNumber() {
        return socialSecurityNumber;
    }

    /**
     * Sets social security number. Throws exception when socialSecurityNumber is null.
     *
     * @param socialSecurityNumber the social security number
     */
    public void setSocialSecurityNumber(String socialSecurityNumber) {
        if(socialSecurityNumber != null) {
            this.socialSecurityNumber= socialSecurityNumber;
        } else {throw new IllegalArgumentException("Social security number cannot be null.");}
    }

    /**
     * Gets diagnosis.
     *
     * @return the diagnosis
     */
    public String getDiagnosis() {
        return diagnosis;
    }

    /**
     * Sets diagnosis. Throws exception when diagnosis is null.
     *
     * @param diagnosis the diagnosis
     */
    public void setDiagnosis(String diagnosis) {
        if(diagnosis != null) {
            this.diagnosis = diagnosis;
        } else {throw new IllegalArgumentException("Diagnosis cannot be null.");}
    }
}
