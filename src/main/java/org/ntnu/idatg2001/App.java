package org.ntnu.idatg2001;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import org.apache.log4j.Logger;
import java.io.IOException;

/**
 * JavaFX App
 */
public class App extends Application {

    private static Scene scene;

    private static final Logger LOGGER = Logger.getLogger(App.class.getName());

    @Override
    public void start(Stage stage) throws IOException {
        LOGGER.info("Starting main stage.");
        try {
            scene = new Scene(loadFXML("primary"), 640, 480);
            stage.setTitle("Patient Register");
            stage.setScene(scene);
            stage.show();
        }catch (NullPointerException nu){
            LOGGER.error("NullPointerException when trying to start main stage."+nu.getMessage());
        }catch(IOException io){
            LOGGER.error("IOException when trying to start main stage."+io.getMessage());
        }
    }

    private static Parent loadFXML(String fxml) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource(fxml + ".fxml"));
        return fxmlLoader.load();
    }

    /**
     * The entry point of application.
     *
     * @param args the input arguments
     */
    public static void main(String[] args) {
        launch();
    }


}