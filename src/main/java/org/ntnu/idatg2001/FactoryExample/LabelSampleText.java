package org.ntnu.idatg2001.FactoryExample;
import javafx.scene.control.Label;

/**
 * The type Label sample text.
 */
public class LabelSampleText extends LabelExample {

    @Override
    public Label createLabel() {
        return new Label("Sample Text");
    }
}
