package org.ntnu.idatg2001;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PatientRegisterTest {
    PatientRegister patientRegister;
    Patient patient;

    @BeforeEach
    void createTestData() {
        this.patientRegister = new PatientRegister();
        this.patient = new Patient("Rob","Matthews","Ronda","01928343291","Covid");
        patientRegister.getPatientList().add(patient);
    }

    @Test
    @DisplayName("Positive test for getting the patient register")
    void getPatientListTest() {
        assertEquals(patientRegister.getPatientList().size(),1);
    }

    @Test
    @DisplayName("Positive test for the addPatient method")
    void addPatientMethodTest() {
        try{
            patientRegister.addPatient("Manny","Peterson","Greg","940392309","Rash");
        }catch(NullPointerException | IllegalArgumentException nu){
        }
        assertEquals(patientRegister.getPatientList().size(),2);
    }

    @Test
    @DisplayName("Negative test for adding null patient")
    void addNullPatientTest() {
        try {
            patientRegister.getPatientList().add(new Patient(null,null,null,null,null));
        }catch (NullPointerException nu){
            assertEquals(nu.getMessage(),"First name cannot be null.");
        }
        assertEquals(patientRegister.getPatientList().size(),1);

    }

    @Test
    @DisplayName("Negative test for adding null patient with addPatient method")
    void addNullPatientWithAddPatientMethodTest() {
        try {
            patientRegister.addPatient(null,null,null,null,null);
        }catch (NullPointerException nu){
            assertEquals(nu.getMessage(),"First name cannot be null.");
        }
        assertEquals(patientRegister.getPatientList().size(),1);

    }

    @Test
    @DisplayName("Negative test for adding blank patient")
    void addBlankPatientTest() {
        try {
            patientRegister.getPatientList().add(new Patient("","","","",""));
        }catch (IllegalArgumentException nu){
            assertEquals(nu.getMessage(),"First name cannot be blank.");
        }
        assertEquals(patientRegister.getPatientList().size(),1);
    }

    @Test
    @DisplayName("Negative test for adding blank patient")
    void addBlankPatientWithAddPatientMethodTest() {
        try {
            patientRegister.addPatient("","","","","");
        }catch (IllegalArgumentException nu){
            assertEquals(nu.getMessage(),"First name cannot be blank.");
        }
        assertEquals(patientRegister.getPatientList().size(),1);
    }


}