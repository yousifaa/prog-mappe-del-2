package org.ntnu.idatg2001;

import java.io.*;
import java.net.URL;
import java.time.LocalDate;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;
import org.apache.log4j.Logger;

/**
 * The type Primary controller.
 */
public class PrimaryController implements Initializable {

    private static final Logger LOGGER = Logger.getLogger(PrimaryController.class.getName());

    private static PatientRegister patientRegister;

    private ObservableList<Patient> patientListWrapper;

    private static Patient selectedPatient;

    /**
     * The Alert template. Used to create alerts when needed.
     */
    Alert alertTemplate = new Alert(Alert.AlertType.INFORMATION);

    /**
     * The Patient table view.
     */
    @FXML
    TableView<Patient> patientTableView;
    /**
     * The First name column.
     */
    @FXML
    TableColumn<Patient,String> firstNameColumn;
    /**
     * The Last name column.
     */
    @FXML
    TableColumn<Patient,String> lastNameColumn;
    /**
     * The General practitioner column.
     */
    @FXML
    TableColumn<Patient,String> generalPractitionerColumn;
    /**
     * The Social security number column.
     */
    @FXML
    TableColumn<Patient,String> socialSecurityNumberColumn;
    /**
     * The Add.
     */
    @FXML
    Button add;
    /**
     * The Edit.
     */
    @FXML
    Button edit;
    /**
     * The Remove.
     */
    @FXML
    Button remove;
    /**
     * The Exit app.
     */
    @FXML
    MenuItem exitApp;
    /**
     * The Status bar.
     */
    @FXML
    Label statusBar;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        LOGGER.info("Initializing main controller.");
        patientRegister = new PatientRegister();
        patientListWrapper = FXCollections.observableArrayList();

        firstNameColumn = new TableColumn<>("First Name");
        lastNameColumn = new TableColumn<>("Last Name");
        generalPractitionerColumn = new TableColumn<>("General Practitioner");
        socialSecurityNumberColumn = new TableColumn<>("Social Security Number");

        firstNameColumn.setCellValueFactory(new PropertyValueFactory<Patient, String>("firstName"));
        lastNameColumn.setCellValueFactory(new PropertyValueFactory<Patient, String>("lastName"));
        generalPractitionerColumn.setCellValueFactory(new PropertyValueFactory<Patient, String>("generalPractitioner"));
        socialSecurityNumberColumn.setCellValueFactory(new PropertyValueFactory<Patient, String>("socialSecurityNumber"));
        updateObservableList();
        patientTableView.setItems(getPatientListWrapper());
        patientTableView.getColumns().addAll(firstNameColumn, lastNameColumn, generalPractitionerColumn, socialSecurityNumberColumn);
    }

    /**
     * Wraps patient register ArrayList to an ObservableList for use in TableView.
     */
    private ObservableList<Patient> getPatientListWrapper() {
        LOGGER.info("Wrapping patient list to ObservableList.");
        patientListWrapper = FXCollections.observableArrayList(this.patientRegister.getPatientList());
        return patientListWrapper;
    }

    /**
     * Update observable list so that it shows all Patients in TableView.
     */
    public void updateObservableList() {
        LOGGER.info("Patient observable list updating.");
        this.patientListWrapper.setAll(this.patientRegister.getPatientList());
    }

    /**
     * Gets patient register.
     *
     * @return the patient register
     */
    public static PatientRegister getPatientRegister() {
        return patientRegister;
    }

    /**
     * Open add new patient window.
     *
     * @throws IOException the io exception
     */
    public void openAddNewPatient() throws IOException {
        Stage stage = new Stage();
        try {
            FXMLLoader fxmlLoader;
            fxmlLoader = new FXMLLoader(getClass().getResource("addPatient.fxml"));
            stage.setTitle("Add New Patient");
            stage.initModality(Modality.APPLICATION_MODAL);
            Parent root1 = (Parent) fxmlLoader.load();
            stage.setScene(new Scene(root1));
            stage.show();
            stage.setOnHiding(evt -> {
                updateObservableList();
                statusBar.setText(AddPatientController.statusMessage);
            });
        } catch (IOException i) {
            LOGGER.error("IOException while trying to Open Add Patient window.");
        }
    }

    /**
     * Open edit existing patient window.
     *
     * @throws IOException the io exception
     */
    public void openEditExistingPatient() throws IOException {
        Stage stage = new Stage();
        try {

            FXMLLoader fxmlLoader;
            fxmlLoader = new FXMLLoader(getClass().getResource("editPatient.fxml"));
            stage.setTitle("Edit an Existing Patient");
            setSelectPatient();
            if (selectedPatient == null) {
                alertTemplate.setHeaderText("No patient is selected.");
                alertTemplate.setContentText("Please select a patient to edit.");
                alertTemplate.show();
            }
            else {
                stage.initModality(Modality.APPLICATION_MODAL);
                Parent root1 = (Parent) fxmlLoader.load();
                stage.setScene(new Scene(root1));
                stage.show();
            }
        }catch(IOException i){
            LOGGER.error("IOException while trying to Open Edit Patient window.");
        }
        stage.setOnHiding(evt -> {updateObservableList(); statusBar.setText(EditPatientController.statusMessage);});
    }


    /**
     * Remove patient in list method. Removes a patient from the ArrayList.
     */
    public void removePatientInList() {
            setSelectPatient();
            if (selectedPatient==null){
                LOGGER.info("No patient is selected, sending alert.");
                alertTemplate.setHeaderText("No patient is selected.");
                alertTemplate.setContentText("Please select a patient to remove.");
                alertTemplate.show();
            } else {
                LOGGER.info("Selected patient being removed.");
                try {
                    patientRegister.getPatientList().remove(selectedPatient);
                    updateObservableList();
                    statusBar.setText("Patient removed.");
                }catch(NullPointerException nu) {
                    LOGGER.error("NullPointerException while trying to remove patient."+nu.getMessage());
                    statusBar.setText("Patient not removed.");
                }
            }
    }

    /**
     * Show about section when pressing about MenuItem.
     */
    public void showAbout() {
        LOGGER.info("Showing about section.");
        alertTemplate.setHeaderText("Patient Register \nv1.0");
        alertTemplate.setContentText("An application to store patient data. \nCreated by Yousif Almallah \n" + LocalDate.now());
        alertTemplate.show();
    }

    /**
     * Sets select patient in the TableView so they can be edited/ removed.
     */
    public void setSelectPatient() {
        LOGGER.info("Setting selected patient.");
        selectedPatient = patientTableView.getSelectionModel().getSelectedItem();
    }

    /**
     * Gets selected patient in the TableView so they can be edited/ removed.
     *
     * @return the selected patient
     */
    public static Patient getSelectedPatient() {
        LOGGER.info("Getting selected patient.");
        return selectedPatient;
    }

    /**
     * Import from csv file. Opens filechooser and determines what information the patients have in the csv file.
     * Illegal argument patients (null/blank or no first name and last name) are counted and not added to the ArrayList.
     */
    public void importFromCSV() {
        try {
            LOGGER.info("Starting importing method.");
            FileChooser fileChooser = new FileChooser();
            fileChooser.setTitle("Select .CSV file");
            fileChooser.getExtensionFilters().addAll(new FileChooser.ExtensionFilter("csv", "*.csv"));
            Stage stage = (Stage) add.getScene().getWindow();

            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(
                    new FileInputStream(fileChooser.showOpenDialog(stage)), "UTF-8"));
            String currentRow;
            bufferedReader.readLine();
            int illegalPatientCounter = 0;
            LOGGER.info("Importing file from .CSV.");
            while ((currentRow = bufferedReader.readLine()) != null) {
                try {
                    String[] data = currentRow.split(";");
                    //Determines what data the patient has.
                    if (data.length==5){
                        patientRegister.addPatient(data[0],data[1],data[2],data[3],data[4]);
                    }else if (data.length==4){
                        patientRegister.addPatient(data[0],data[1],data[2],data[3],"");
                    }else if (data.length==3){
                        patientRegister.addPatient(data[0],data[1],data[2],"","");
                    }else if (data.length==2){
                        patientRegister.addPatient(data[0],data[1],"","","");
                    }else {throw new IllegalArgumentException("First/ last name empty");}
                }catch (IllegalArgumentException il){
                    //Catches patients with illegal arguments and counts them.
                    illegalPatientCounter++;
                }
            }
            if (illegalPatientCounter==0){
                LOGGER.info("Import successful.");
                statusBar.setText("Patients imported from .CSV file.");
            }else {
                LOGGER.warn(illegalPatientCounter + " patients not added from .CSV file due to IllegalArgumentException.");
                statusBar.setText("Some patients (" + illegalPatientCounter + ") were not imported due to wrong information.");
            }
            bufferedReader.close();
            updateObservableList();
        } catch (FileNotFoundException fi) {
            statusBar.setText("Patients not imported because file was not found.");
            LOGGER.error("FileNotFoundException while trying to import. Error message: " + fi.getMessage());;
        } catch (IOException io) {
            statusBar.setText("Patients not imported.");
            LOGGER.info("IOException while trying to import." + io.getMessage());;
        } catch (NullPointerException nu) {
            statusBar.setText("Patients not imported because no file was selected.");
            LOGGER.error("NullPointerException while trying to import. Error message: " + nu.getMessage());
        } catch (RuntimeException ru) {
            statusBar.setText("Patients not imported.");
            LOGGER.error("RuntimeException while trying to import. Error message: " + ru.getMessage());
        }
    }

    /**
     * Export to csv file. Opens file chooser so user can input file name and location.
     */
    public void exportToCSV() {
        try {
            LOGGER.info("Starting export method.");
            FileChooser fileChooser = new FileChooser();
            fileChooser.setTitle("Save .CVS file");
            fileChooser.getExtensionFilters().addAll(new FileChooser.ExtensionFilter("csv", "*.csv"));
            Stage stage = (Stage) add.getScene().getWindow();

            BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(
                    new FileOutputStream(fileChooser.showSaveDialog(stage)), "UTF-8"));

            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("First Name;Last Name;General Practitioner;Social Security Number;Diagnosis");
            bufferedWriter.write(stringBuilder.toString());
            StringBuilder stringBuilder1;

            LOGGER.info("Exporting to .CSV.");
            for (Patient patient : patientRegister.getPatientList()){
                stringBuilder1 = new StringBuilder();
                stringBuilder1.append(patient.getFirstName()+";"+patient.getLastName()+";"+patient.getGeneralPractitioner()+";"+patient.getSocialSecurityNumber()+";"+patient.getDiagnosis());
                bufferedWriter.newLine();
                bufferedWriter.write(stringBuilder1.toString());
            }

            bufferedWriter.flush();
            bufferedWriter.close();

            updateObservableList();
            statusBar.setText("Patients exported to .CSV file.");
        } catch (IOException io) {
            statusBar.setText("Patients not exported.");
            LOGGER.error("IOException while trying to export. Error message: " + io.getMessage());
        } catch (NullPointerException nu) {
            statusBar.setText("Patients not exported due to exiting export window.");
            LOGGER.error("NullPointerException while trying to export. Error message: " + nu.getMessage());
        }
    }

    /**
     * Exits app when pressing on exit MenuItem.
     */
    public void exitApp() {
        LOGGER.info("Exiting application.");
        System.exit(0);
    }
}
