package org.ntnu.idatg2001;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PatientTest {
    Patient normalPatient;
    Patient nullPatient;
    Patient emptyStringPatient;

    @BeforeEach
    void createTestData(){
        this.normalPatient = new Patient("Man","This","Alex","09182930182","Cold");
    }

    @Test
    @DisplayName("Positive test for changing firstName")
    void setFirstNameTest() {
        normalPatient.setFirstName("Rodger");
        assertEquals("Rodger",normalPatient.getFirstName());
    }

    @Test
    @DisplayName("Positive test for setting GeneralPractitioner")
    void setGeneralPractitionerTest() {
        normalPatient.setGeneralPractitioner("Kiran");
        assertEquals("Kiran",normalPatient.getGeneralPractitioner());
    }

    @Test
    @DisplayName("Negative test for setting null patient")
    void newNullPatientTest() {
        try {
            nullPatient = new Patient(null,null,null,null,null);
        }catch (NullPointerException nu) {
            assertEquals(nu.getMessage(),"First name cannot be null.");
        }
    }

    @Test
    @DisplayName("Negative test for setting empty string patient")
    void newEmptyStringPatientTest() {
        try {
            emptyStringPatient = new Patient("", "", "", "", "");
        } catch (IllegalArgumentException il) {
            assertEquals(il.getMessage(),"First name cannot be blank.");
        }
    }
}