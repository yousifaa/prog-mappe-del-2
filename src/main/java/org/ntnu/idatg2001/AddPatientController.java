package org.ntnu.idatg2001;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import org.apache.log4j.Logger;
import java.net.URL;
import java.util.ResourceBundle;

/**
 * The type Add patient controller.
 */
public class AddPatientController implements Initializable {

    /**
     * The First name.
     */
    @FXML
    TextField firstName;
    /**
     * The Last name.
     */
    @FXML
    TextField lastName;
    /**
     * The General practitioner.
     */
    @FXML
    TextField generalPractitioner;
    /**
     * The Social security number.
     */
    @FXML
    TextField socialSecurityNumber;
    /**
     * The Diagnosis.
     */
    @FXML
    TextField diagnosis;
    /**
     * The Ok button.
     */
    @FXML
    Button okButton;
    /**
     * The Cancel button.
     */
    @FXML
    Button cancelButton;

    /**
     * The Alert.
     */
    Alert alert;

    private static final Logger LOGGER = Logger.getLogger(AddPatientController.class.getName());

    /**
     * The constant statusMessage used to send a status message to the main controller.
     */
    public static String statusMessage;

    /**
     * The Patient.
     */
    Patient patient;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle){
        LOGGER.info("Initializing AddPatientController.");
        statusMessage = "Patient not added";
    }

    /**
     * Add patient to list. Gets information inputted in Labels and creates Patient object.
     */
    public void addPatientToList() {
        LOGGER.info("Adding patient to list.");
        try {
            patient = new Patient(firstName.getText(),lastName.getText(), generalPractitioner.getText(),socialSecurityNumber.getText(),diagnosis.getText());
            PrimaryController.getPatientRegister().getPatientList().add(patient);
            statusMessage = "Patient added";
        }catch (IllegalArgumentException | NullPointerException nu) {
            LOGGER.error("NullPointerException or IllegalArgumentException while trying to edit patient. Error message is: " + nu.getMessage());
            alert = new Alert(Alert.AlertType.ERROR);
            alert.setHeaderText("Error while trying to add patient.");
            alert.setContentText("Patient's first and last name cannot be blank.");
            alert.show();
        }
        closeStage();
    }

    /**
     * Close stage.
     */
    public void closeStage() {
        LOGGER.info("Closing Add Patient stage.");
        Stage stage = (Stage) cancelButton.getScene().getWindow();
        stage.close();
    }
}