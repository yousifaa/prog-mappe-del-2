package org.ntnu.idatg2001.FactoryExample;
import javafx.scene.control.Label;

/**
 * The type Label lorem ipsum.
 */
public class LabelLoremIpsum extends LabelExample {

    @Override
    public Label createLabel() {
        return new Label("Lorem Ipsum");
    }
}
