package org.ntnu.idatg2001.FactoryExample;

/**
 * The type Label factory. An example of factory design patterns.
 */
public class LabelFactory {

    /**
     * Create label example from input.
     *
     * @param input the input
     * @return the label example
     */
    public LabelExample createLabelExample(String input) {
        LabelExample labelExample;
        switch (input) {
            case "Sample Text":
                labelExample = new LabelSampleText();
                break;
            case "Lorem Ipsum":
                labelExample = new LabelSampleText();
                break;
            default:
                throw new IllegalArgumentException("Input does not match any label types.");
        }
        return labelExample;
    }
}
