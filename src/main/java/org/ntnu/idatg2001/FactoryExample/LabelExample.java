package org.ntnu.idatg2001.FactoryExample;

import javafx.scene.control.Label;

/**
 * The type Label example.
 */
public abstract class LabelExample {

    /**
     * Create label label.
     *
     * @return the label
     */
    public abstract Label createLabel();
}
