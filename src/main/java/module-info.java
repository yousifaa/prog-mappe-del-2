module org.ntnu.idatg2001 {
    requires javafx.controls;
    requires javafx.fxml;
    requires log4j;

    opens org.ntnu.idatg2001 to javafx.fxml;
    exports org.ntnu.idatg2001;
}